﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace The_2D_Array
{
    public class Program
    {

        public static  int EvenPositionedSum(int[,] arr)
        {
            int sum = 0;
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if((i+j)%2==0)
                    {
                        sum+=arr[i,j];
                    }

                }
            }
            return (sum);
        }
        public static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Intilize i-demension capacity");
                int n = Int32.Parse(Console.ReadLine());
                Console.WriteLine("Intilize j-demension capacity");
                int k = Int32.Parse(Console.ReadLine());
                Random rand = new Random();
                int[,] array = new int[n, k];
                for (int i = 0; i < array.GetLength(0); i++)
                {
                    for (int j = 0; j < array.GetLength(1); j++)
                    {
                        array[i, j] = rand.Next(-1000, 1000);
                        Console.WriteLine($"array[{i},{j}]=" + array[i, j]);
                    }
                }
                Console.WriteLine("Sum of elements on even position:" + EvenPositionedSum(array));
            }
            catch(FormatException)
            {
                Console.WriteLine("Unable to parse input values");
            }

        }
    }
}