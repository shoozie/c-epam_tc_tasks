﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rectangle
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Initialize a");
                int a = Int32.Parse(Console.ReadLine());
                Console.WriteLine("initialize b");
                int b = Int32.Parse(Console.ReadLine());
                try
                {
                    if (a <= 0 || b <= 0)
                    {
                        throw new Exception("Values of a and b must be greater than zero"); //generating exeption in case of inappropriate input
                    }
                    else
                    {
                        Console.WriteLine("Square of rectangle equals:" + a * b); //appropriate case
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message); // showing exeption Message
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Unable to parse input values");
            }
           
            
        }
    }
}