﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Non_Negative_Sum
{
    public class Program
    {

        public static double NonNegSum(double[] arr)
        {
            double sum = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if(arr[i] >= 0)
                {
                    sum += arr[i];
                }
            }
            return sum;
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("Initilize array capacity:");
            try
            {
                int n = Int32.Parse(Console.ReadLine());
                Random random = new Random();
                double[] array = new double[n];
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = random.Next(-100, 100);
                    Console.WriteLine($"array[{i}]=" + array[i]);
                }
                Console.WriteLine("Sum of non negative items is:" + NonNegSum(array));
            }
            catch(FormatException)
            {
                Console.WriteLine("Unable to parse input values");
            }

        }
    }
}