﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Doubler
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Insert first string");
            string str1 = Console.ReadLine();
            Console.WriteLine("Insert second string");
            string str2 = Console.ReadLine();
            if (!string.IsNullOrEmpty(str1) && !string.IsNullOrEmpty(str2))
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < str1.Length; i++)
                {
                    if (str2.Contains(str1[i]) && str1[i] != ' ')
                    {
                        sb.Append(str1[i], 2);
                    }
                    else
                    {
                        sb.Append(str1[i], 1);
                    }


                }

                Console.WriteLine(sb.ToString());

            }
            else
            {
                Console.WriteLine("Input is NULL");
            }
        }
    }
}